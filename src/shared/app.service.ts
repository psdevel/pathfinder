import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class AppService {
  constructor(private http: HttpClient) {}

  getCountryNames(filterValue: string): Observable<{name: string, alpha3Code: string}[]> {
    const options = {params: new HttpParams().set('fields', 'name;alpha3Code')};
    return this.http.get<{name: string, alpha3Code: string}[]>(environment.countryApi + filterValue, options);
  }

  getTravelPath(
    originCountryCode: string,
    destinationCountryCode: string
  ): Observable<{ countryName: string, coordinates: { lat: number, lng: number } }[]> {

    /** TODO: Remove after backendApi set */
    if (!environment.backendApi) {
      if (originCountryCode === 'FRA' && destinationCountryCode === 'LVA') {
        return this._getTravelPathMock();
      } else {
        return this._getTravelPathErrorMock();
      }
    }

    return this.http.get<{countryName: string, coordinates: {lat: number, lng: number}}[]>(
      `${environment.backendApi}/routing/${originCountryCode}/${destinationCountryCode}`
    );
  }

  private _getTravelPathMock(): Observable<{ countryName: string, coordinates: { lat: number, lng: number } }[]> {
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(
          [
            {
              countryName: 'France',
              coordinates: {
                lat: 46.2276,
                lng: 2.2137
              }
            },
            {
              countryName: 'Germany',
              coordinates: {
                lat: 51.1657,
                lng: 10.4515
              }
            },
            {
              countryName: 'Poland',
              coordinates: {
                lat: 51.9194,
                lng: 19.1451
              }
            },
            {
              countryName: 'Lithuania',
              coordinates: {
                lat: 55.1694,
                lng: 23.881
              }
            },
            {
              countryName: 'Latvia',
              coordinates: {
                lat: 56.8796,
                lng: 24.6032
              }
            }
          ]
        );
      }, 500);
    });
  }

  private _getTravelPathErrorMock(): Observable<{countryName: string, coordinates: {lat: number, lng: number}}[]> {
    return new Observable(observer => {
      setTimeout(() => {
        observer.error(
          {
            error: {
              code: 1,
              message: 'Direct land route between the two countries was not found.'
            }
          }
        );
      }, 500);
    });
  }

}
