import { AbstractControl, ValidatorFn } from '@angular/forms';

export function nameCodeValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    let invalid = true;
    if (
      control.value !== null &&
      typeof control.value === 'object' &&
      control.value.hasOwnProperty('alpha3Code')
    ) {
      invalid = false;
    }

    return invalid ? {codeName: {value: 'Please select country'}} : null;
  };
}
