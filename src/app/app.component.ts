import { Component, OnInit } from '@angular/core';
import { AppService } from '../shared/app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { nameCodeValidator } from '../shared/name-code-object.directive';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  path: { countryName: string, coordinates: { lat: number, lng: number } }[];
  mapStartPosition: number[] = [49.75, 15.5];

  originCountryOption: {name: string, alpha3Code: string} = null;
  destinationCountryOption: {name: string, alpha3Code: string} = null;

  filteredOptions: Observable<{name: string, alpha3Code: string}[]>;

  travelPath: FormGroup = this.fb.group({
    originCountry: this.fb.control(this.originCountryOption, [Validators.required, nameCodeValidator()]),
    destinationCountry: this.fb.control(this.destinationCountryOption, [Validators.required, nameCodeValidator()]),
  });

  constructor(
    private fb: FormBuilder,
    private service: AppService,
    public message: MatSnackBar,
  ) {}

  ngOnInit() {
    this._onFormControlChange('originCountry');
    this._onFormControlChange('destinationCountry');
  }

  onSubmit() {
    this._findPath();
  }

  displayName(value: {name: string, alpha3Code: string}): string {
    if (value) {
      return value.name;
    }
    return '';
  }

  hasBackendApi(): boolean {
    return !!environment.backendApi;
  }

  clearPath(): void {
    this.path = [];
  }

  private _findPath() {
    this.service.getTravelPath(
      this.travelPath.get('originCountry').value.alpha3Code.toString().toUpperCase(),
      this.travelPath.get('destinationCountry').value.alpha3Code.toString().toUpperCase()
    )
    .subscribe(
      res => {
        this.message.dismiss();
        this.path = res;
        this.mapStartPosition = [this.path[0].coordinates.lat, this.path[0].coordinates.lng];
      },
      error => {
        this.clearPath();
        this.message.open(error.error.message ? error.error.message : 'Something goes wrong, try again later.', 'OK');
      }
    );
  }

  private _onFormControlChange(formControlName: string) {
    this.travelPath.get(formControlName).valueChanges
    .pipe(
      debounceTime(250),
      distinctUntilChanged(),
    )
    .subscribe(value => {
      this.clearPath();
      this.message.dismiss();
      if (typeof value === 'string' && value.length > 0) {
        this.filteredOptions = this.service.getCountryNames(value.toLowerCase())
        .pipe(
          catchError(() => {
            this.message.open('Country not found', null, {duration: 3000});
            return [];
          })
        );
      }
    });
  }
}
