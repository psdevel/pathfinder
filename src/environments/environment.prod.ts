export const environment = {
  production: true,
  countryApi: 'https://restcountries.eu/rest/v2/name/',
  backendApi: null,
  googleApiKey: null
};
