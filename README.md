# Travel Path

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9. as Frontend Developer Test.

## Before all

Run `npm install` in project root to install npm packages.

Set own google cloud platform `API_KEY` in [environments](src/environments) files. More info [how to](https://developers.google.com/places/web-service/get-api-key).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Usage

Start write country name of origin then select it from dropdown. Destination country as well.

Both inputs are required and have to be selected from list to be sure of data validity. 

After submit with enter or click on `Search for path` button see the polylines path with markers on the map.

## Ideas for improvement

- set backend url in [environments](src/environments), then remove mockup data.
- use shared entity model instead of `{countryName: string, coordinates: {lat: number, lng: number}}`
- clear previous searched country name when switching inputs
- create e2e tests
- create unit tests
- improve UX & UI using style structure and BEM & Atomic design
- center map to center of whole path
- fix zoom map to obtain whole path
- map init with center based on user geolocation

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
