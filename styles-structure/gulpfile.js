// //////////////////////////////////
// REQUIRES
// //////////////////////////////////
const
    gulp = require('gulp'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    uglify_js = require('gulp-uglify'),
    uglify_css = require('gulp-clean-css'),
    sass = require('gulp-sass'),
    gulp_inject = require('gulp-inject'),
    merge = require('merge-stream');

// //////////////////////////////////
// VARIABLES
// //////////////////////////////////
const
    appFolder = './',
    sourceFolder = 'src/',
    destinationFolder = 'dist/',
    finalCssName = 'styles';

// //////////////////////////////////
// ENVIRONMENT VARIABLES
// //////////////////////////////////
let
    cssDestinationPath = destinationFolder,
    projectAssetsCss = '../src/';

// //////////////////////////////////
// TASKS
// //////////////////////////////////
gulp.task('process-scss', function () {
    return gulp.src(sourceFolder + 'scss/style.scss')
        .pipe(sass())
        .pipe(rename(finalCssName + '.css'))
        .pipe(gulp.dest(cssDestinationPath))
        .pipe(uglify_css({compatibility: 'ie8'}))
        .pipe(rename(finalCssName + '.min.css'))
        .pipe(gulp.dest(cssDestinationPath));
});
gulp.task('copy-final-files', function () {
    let css = gulp.src(destinationFolder + '*.min.css')
        .pipe(gulp.dest(projectAssetsCss));
    console.log('Copying files into ' + projectAssetsCss);
    return css;
});

// //////////////////////////////////
// COMPUND TASKS
// //////////////////////////////////
gulp.task('process-files', gulp.series(
    ['process-scss']
));
gulp.task('export', gulp.series(
    ['process-files', 'copy-final-files']
));

// //////////////////////////////////
// WATCH TASKS
// //////////////////////////////////
gulp.task('ng-watch', function () {
    gulp.watch(sourceFolder + 'scss/**/*.scss', gulp.series(['process-scss', 'copy-final-files']));
});

// //////////////////////////////////
// DEFAULT TASKS
// //////////////////////////////////
gulp.task('default', gulp.series(
    ['export']
));

gulp.task('help', function(done) {
    console.log('gulp or gulp default           : Run default tasks. Default: export');
    console.log('gulp process-scss              : Rebuild scss and creates uglyfied output into dist folder');
    console.log('gulp export                    : Process scss and save them into: ' + projectAssetsCss);
    console.log('gulp ng-watch                  : Watch changes in scss files then run export');
    done();
});
